// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:lista_de_tarefas/main.dart';

void main() {
  testWidgets('Teste de Widget', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(Home());
    final add = find.byKey(Key('add'));

    expect(find.text("Lista de Tarefas"), findsOneWidget);
    expect(find.text("Nova Tarefa"), findsOneWidget);
    expect(add, findsOneWidget);

    final tarefaFinder = find.byKey(Key('0'));
    final textField = find.byKey(Key('tf'));
    expect(tarefaFinder, findsNothing);
    expect(textField, findsOneWidget);
    await tester.enterText(textField, "Tarefa 0");

    await tester.tap(add);
    await tester.pump();
    expect(tarefaFinder, findsOneWidget);
    CheckboxListTile checkboxListTile =
        tester.widget<CheckboxListTile>(tarefaFinder);
    expect(checkboxListTile.title.toString(), Text("Tarefa 0").toString());
    expect(checkboxListTile.value, false);
    expect(find.byIcon(Icons.error), findsOneWidget);

    await tester.tap(tarefaFinder);
    await tester.pump();
    expect(find.byIcon(Icons.check), findsOneWidget);

    await tester.drag(tarefaFinder, Offset(500.0, 0.0));
    await tester.pumpAndSettle();
    expect(tarefaFinder, findsNothing);

    final snackBarActionFinder = find.byKey(Key('snackBarAction'));
    expect(snackBarActionFinder, findsOneWidget);
    await tester.tap(snackBarActionFinder);
    await tester.pump();
    expect(tarefaFinder, findsOneWidget);
  });
}
