import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:lista_de_tarefas/file_manager.dart';

void main() {
  FileManager fileManager = new FileManager();
  List toDoListTeste = [
    {"title": "Tarefa 0", "check": false},
    {"title": "Tarefa 1", "check": false}
  ];
  test("Instânciar FileManager", () {
    expect(fileManager.toDoList, []);
  });

  test("FileManager - saveData e AddToList", () async {
    fileManager.toDoList.add({"title": "Tarefa 0", "check": false});
    fileManager.saveData();
    final data = await fileManager.readData();
    final tarefasExpected = json.encode([
      {"title": "Tarefa 0", "check": false}
    ]);
    expect(data, tarefasExpected);

    fileManager.controller.text = "Tarefa 1";
    fileManager.addToDo();
    expect(fileManager.toDoList, toDoListTeste);
  });

  test("FileManager - getFile", () async {
    final file = await fileManager.getFile();
    String tarefasExpected = json.encode([
      {"title": "Tarefa 0", "check": false},
      {"title": "Tarefa 1", "check": false}
    ]);
    expect(file.readAsStringSync(), tarefasExpected);
  });

  test("FileManager - readData", () async {
    final data = await fileManager.readData();
    String tarefasExpected = json.encode([
      {"title": "Tarefa 0", "check": false},
      {"title": "Tarefa 1", "check": false}
    ]);
    expect(data, tarefasExpected);
  });
}
