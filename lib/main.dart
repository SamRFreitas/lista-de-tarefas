import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:lista_de_tarefas/file_manager.dart';

void main() {
  runApp(Home());
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController toDoController = TextEditingController();

  FileManager fileManager = new FileManager();

  void updateList() {
    setState(() {
      fileManager.addToDo();
    });
  }

  @override
  void initState() {
    super.initState();
    fileManager.readData().then((data) {
      setState(() {
        fileManager.toDoList = json.decode(data);
      });
    });
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 1));

    setState(() {
      fileManager.toDoList.sort((a, b) {
        if (a["check"] && !b["check"]) {
          return 1;
        } else if (!a["check"] && b["check"]) {
          return -1;
        } else {
          return 0;
        }
      });

      fileManager.saveData();
    });

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Lista de Tarefas"),
          backgroundColor: Colors.blueAccent,
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(17, 1, 7, 1),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      key: Key('tf'),
                      controller: fileManager.controller,
                      decoration: InputDecoration(
                          labelText: "Nova Tarefa",
                          labelStyle: TextStyle(color: Colors.blueAccent)),
                    ),
                  ),
                  RaisedButton(
                    key: Key("add"),
                    color: Colors.blueAccent,
                    child: Text("Add"),
                    textColor: Colors.white,
                    onPressed: updateList,
                  )
                ],
              ),
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: _refresh,
                child: ListView.builder(
                  padding: EdgeInsets.only(top: 10),
                  itemCount: fileManager.toDoList.length,
                  itemBuilder: buildItem,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildItem(context, index) {
    return Dismissible(
      key: Key(DateTime.now().millisecondsSinceEpoch.toString()),
      background: Container(
        color: Colors.red,
        child: Align(
          alignment: Alignment(-0.9, 0.0),
          child: Icon(
            Icons.delete,
            color: Colors.white,
          ),
        ),
      ),
      direction: DismissDirection.startToEnd,
      child: CheckboxListTile(
        key: Key(index.toString()),
        title: Text(fileManager.toDoList[index]["title"]),
        value: fileManager.toDoList[index]["check"],
        secondary: CircleAvatar(
          backgroundColor: fileManager.toDoList[index]["check"]
              ? Colors.green
              : Colors.blueAccent,
          child: Icon(
            fileManager.toDoList[index]["check"] ? Icons.check : Icons.error,
          ),
        ),
        onChanged: (bool value) {
          setState(() {
            fileManager.toDoList[index]["check"] = value;
            fileManager.saveData();
          });
        },
      ),
      onDismissed: (direction) {
        setState(() {
          fileManager.lastRemoved = Map.from(fileManager.toDoList[index]);
          fileManager.lastRemovePos = index;
          fileManager.toDoList.removeAt(fileManager.lastRemovePos);
          fileManager.saveData();

          final snack = SnackBar(
            content:
                Text("Tarefa ${fileManager.lastRemoved["title"]} removida"),
            action: SnackBarAction(
                key: Key("snackBarAction"),
                label: "Desfazer",
                onPressed: () {
                  setState(() {
                    fileManager.toDoList.insert(
                        fileManager.lastRemovePos, fileManager.lastRemoved);
                  });
                }),
            duration: Duration(seconds: 2),
          );
          Scaffold.of(context).removeCurrentSnackBar();
          Scaffold.of(context).showSnackBar(snack);
        });
      },
    );
  }
}
