import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';

class FileManager {
  List toDoList = [];
  TextEditingController controller = new TextEditingController();
  Map<String, dynamic> lastRemoved;
  int lastRemovePos;

  void addToDo() {
    Map<String, dynamic> newToDo = Map();
    newToDo["title"] = controller.text;
    controller.text = "";
    newToDo["check"] = false;
    toDoList.add(newToDo);
    saveData();
  }

  Future<File> getFile() async {
    final directory = await getApplicationDocumentsDirectory();
    return File("${directory.path}/tarefas.json");
  }

  Future<File> saveData() async {
    String tarefas = json.encode(toDoList);

    final file = await getFile();
    return file.writeAsString(tarefas);
  }

  Future<String> readData() async {
    try {
      final file = await getFile();
      return file.readAsString();
    } catch (e) {
      return null;
    }
  }
}
